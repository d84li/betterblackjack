# BetterBlackjack
Automated blackjack game and card dealer with computer vision to keep track of cards. Created using a Raspberry Pi.

<img src="./docs/blackjack-machine.png">

User Interface
<ul>
    <li>User interacts with the game using the LED screen and two buttons on the breadboard.</li>
</ul>
Rollers
<ul>
    <li>Cards are held in a 3D printed card holder and rolled out with continuous servos.</li>
</ul>
Image Recognition
<ul>
    <li>A Pi Camera takes an image of the corner of the card, contour comparison algorithms implemented with Pillow and NumPy determine the card value.</li>
</ul>
